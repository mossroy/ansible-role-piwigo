# Ansible role Piwigo

Simple Ansible role to install and configure Piwigo in my self-hosting context

The `piwigo_mysql_password` variable needs to be set.

The following optional variables can be overriden (defaulting to "piwigo") :
*  `piwigo_directory_name`
*  `piwigo_mysql_database_name`
*  `piwigo_mysql_username`
*  `piwigo_apache_site_name`
*  `piwigo_apache_site_alias`
